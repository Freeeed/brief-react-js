import React from "react";

import {
  SafeAreaView,
  StyleSheet,
  KeyboardAvoidingView,
  StatusBar,
  Platform,
} from "react-native";

import { LoginScreen } from "./Screens/LoginScreen/LoginScreen.js";
import { HomeScreen } from "./Screens/HomeScreen/HomeScreen.js";
import { RegisterScreen } from "./Screens/RegisterScreen/RegisterScreen.js";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
        </Stack.Navigator>
      </NavigationContainer>

      {/* <StatusBar barStyle="dark-content" />
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : "position"}
                style={{ flex: 1 }}
                enabled
            >
                <SafeAreaView style={styles.container}>
                    <LoginView />
                </SafeAreaView>
            </KeyboardAvoidingView> */}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f6f6f6",
  },
});

export default App;
