import * as React from 'react';
import { Button, View, Text, TextInput } from 'react-native';
import { styles } from '../RegisterScreen/RegisterScreenStyle.js';

export function RegisterScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Register Screen</Text>
        <TextInput
        style={styles.input}
        placeholder="Prénom..."
        keyboardType="numeric"
        />
        <TextInput
        style={styles.input}
        placeholder="Nom..."
        keyboardType="numeric"
        />
        <TextInput
        style={styles.input}
        placeholder="Adresse Email..."
        keyboardType="numeric"
        />
        <TextInput
        style={styles.input}
        placeholder="Mot de passe..."
        keyboardType="numeric"
        />
        <TextInput
        style={styles.input}
        placeholder="Confirmer votre mot de passe..."
        keyboardType="numeric"
        />
        <Button
          title="S'inscrire"
          onPress={() => navigation.navigate('Login')}
        />
      </View>
    );
}