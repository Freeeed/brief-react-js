import React from "react";
import { View, Text, TouchableOpacity, Image, TextInput } from "react-native";
import { useState } from "react";
import Spinner from "react-native-loading-spinner-overlay";
import axios from "axios";

// Define some colors and default sane values
const utils = {
    colors: { primaryColor: "#af0e66" },
    dimensions: { defaultPadding: 12 },
    fonts: { largeFontSize: 18, mediumFontSize: 16, smallFontSize: 12 },
};

// Define styles here
const styles = {
    innerContainer: {
        marginBottom: 32,
    },
    logotypeContainer: {
        alignItems: "center",
    },
    logotype: {
        maxWidth: 280,
        maxHeight: 100,
        resizeMode: "contain",
        alignItems: "center",
    },
    containerStyle: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#f6f6f6",
    },
    input: {
        height: 50,
        padding: 12,
        backgroundColor: "white",
        borderRadius: 6,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.05,
        shadowRadius: 4,
        marginBottom: utils.dimensions.defaultPadding,
    },
    loginButton: {
        borderColor: utils.colors.primaryColor,
        borderWidth: 2,
        padding: utils.dimensions.defaultPadding,
        alignItems: "center",
        justifyContent: "center",
        margin: 10,
        borderRadius: 6,
    },
    loginButtonText: {
        color: utils.colors.primaryColor,
        fontSize: utils.fonts.mediumFontSize,
        fontWeight: "bold",
    },
    errorMessageContainerStyle: {
        marginBottom: 8,
        backgroundColor: "#fee8e6",
        padding: 8,
        borderRadius: 4,
    },
    errorMessageTextStyle: {
        color: "#db2828",
        textAlign: "center",
        fontSize: 12,
    },
};

export function LoginScreen({navigation}) {
    const [emailState, setEmailState] = useState("");
    const [passwordState, setPasswordState] = useState("");
    const [isAuthorized, setIsAuthorized] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [clientToken, setClientToken] = useState("");
    const [errorsState, setErrorsState] = useState({});

    // const componentWillUnmount = () => {};

    const onUsernameChange = (email) => {
        setEmailState(email);
    };

    const onPasswordChange = (password) => {
        setPasswordState(password);
    };

    const onPressLogin = () => {
        const payload = {
            email: emailState,
            password: passwordState,
        };
        console.log(payload);

        const onSuccess = ({ data }) => {
            // Set JSON Web Token on success
            setClientToken(data.token);
            setIsLoading(false);
            setIsAuthorized(true);
        };
        const onFailure = (error) => {
            console.log(error && error.response);
            // setErrorsState({ errors: error.response.data });
            console.log(errorsState);
            setIsLoading(false);
        };

        // Show spinner when call is made
        setIsLoading(true);

        axios({
            method: "POST",
            url: "http://localhost:8000/authentication_token",
            data: payload,
        })
            .then(onSuccess)
            .catch(onFailure);
    };

    const getErrorMessageByField = () => {
        // Checks for error message in specified field
        // Shows error message from backend
        let message = null;
        if (errorsState.length > 0) {
            message = (
                <View style={styles.errorMessageContainerStyle}>
                    {errorsState.map((item) => (
                        <Text style={styles.errorMessageTextStyle} key={item}>
                            {item}
                        </Text>
                    ))}
                </View>
            );
        }
        return message;
    };
    
    return (
        <View style={styles.containerStyle}>
            <Spinner visible={isLoading} />

            {!isAuthorized ? (
                <View>

                    <TextInput
                        style={styles.input}
                        value={emailState}
                        maxLength={256}
                        placeholder="Identifiant..."
                        autoCapitalize="none"
                        autoCorrect={false}
                        returnKeyType="next"
                        onSubmitEditing={(event) => {
                            event.passwordInput.wrappedInstance.focus();
                            console.log(event);
                        }}
                        onChangeText={onUsernameChange}
                        underlineColorAndroid="transparent"
                        placeholderTextColor="#999"
                    />

                    <TextInput
                        style={styles.input}
                        value={passwordState}
                        maxLength={40}
                        placeholder="Mot de passe..."
                        onChangeText={onPasswordChange}
                        autoCapitalize="none"
                        autoCorrect={false}
                        returnKeyType="done"
                        blurOnSubmit
                        onSubmitEditing={onPressLogin}
                        secureTextEntry
                        underlineColorAndroid="transparent"
                        placeholderTextColor="#999"
                    />

                    {getErrorMessageByField()}

                    {/* {getNonFieldErrorMessage()} */}

                    <TouchableOpacity style={styles.loginButton} onPress={() => navigation.navigate('Home')}>
                        <Text style={styles.loginButtonText}>Se Connecter</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.loginButton} onPress={() => navigation.navigate('Register')}>
                        <Text style={styles.loginButtonText}>S'inscrire</Text>
                    </TouchableOpacity>
                </View>
            ) : (
                <View>
                    <Text>Successfully authorized!</Text>
                </View>
            )}
        </View>
    );
}
