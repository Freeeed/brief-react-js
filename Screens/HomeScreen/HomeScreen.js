import * as React from "react";
import { Button, View, Text } from "react-native";
import { useState, useEffect } from "react";
import axios from "axios";

export function HomeScreen({ navigation }) {
  // exemple d'appel à une api
  const [expenses, setExpenses] = useState([]);
  const [categories, setCategories] = useState([]);
  let usersData = "http://localhost:8000/api/me";
  useEffect(() => {
    axios
      .get(usersData)
      .then((response) => {
        setExpenses(response.data["expenses"]);
        // const event = new Date()
        // console.log(event.toISOString().substring(0,10));
      })
      .catch((error) => {
        console.warn(error);
      });
  }, []);

  const [expensesLast30Days, setExpensesLast30Days] = useState([]);
  let expensesData = `http://localhost:8000/api/me[before]=${new Date().toISOString().substring(0,10)}`
  useEffect(() => {
    axios
      .get(expensesData)
      .then((response) => {
        setExpensesLast30Days(response.data["hydra:member"]);
      })
      .catch((error) => {
        console.warn(error);
      });
  }, []);

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Bienvenue JeanJean</Text>
      <Text>
        Voici les dépenses des 30 dernier jours :
        {/* {expensesLast30Days.map((expense) => {
          return (
            <Text key={expense.id}>
              <ul>
                <li>{expense.amount}</li>
                <li>{expense.description}</li>
                <li>{expense.expenseDate}</li>
              </ul>
            </Text>
          );
        })} */}
        {/* {expenses.map((expense) => {
          return (
            <Text key={expense.id}>
              <ul>
                <li>{expense.amount}</li>
                <li>{expense.description}</li>
                <li>{expense.expenseDate}</li>
              </ul>
            </Text>
          );
        })} */}
      </Text>
      <Button
        title="Se Déconnecter"
        onPress={() => navigation.navigate("Login")}
      />
    </View>
  );
}


